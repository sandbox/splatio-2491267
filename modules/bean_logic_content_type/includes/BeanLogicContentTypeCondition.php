<?php

/**
 * @file
 * Contains BeanLogicContentTypeCondition.
 */

/**
 * Class BeanLogicContentTypeCondition.
 */
class BeanLogicContentTypeCondition extends BeanLogicConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function valueForm($form, &$form_state) {
    return array(
      '#type' => 'select',
      '#options' => node_type_get_names(),
      '#default_value' => $this->value,
      '#required' => TRUE,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function valueFormSubmit($value_values) {
    $this->value = $value_values;
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $return = FALSE;

    if ($node = menu_get_object()) {
      $return = $node->type === $this->value;
    }

    return $this->negate($return);
  }

}
