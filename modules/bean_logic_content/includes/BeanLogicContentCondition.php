<?php

/**
 * @file
 * Contains BeanLogicContentCondition.
 */

/**
 * Class BeanLogicContentCondition.
 */
class BeanLogicContentCondition extends BeanLogicConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function valueForm($form, &$form_state) {
    return array(
      '#type' => 'textfield',
      '#default_value' => $this->value,
      '#required' => TRUE,
      '#size' => 40,
      '#autocomplete_path' => 'bean-logic-content/autocomplete',
    );
  }

  /**
   * {@inheritdoc}
   */
  public function valueFormValidate($value_values, $value_form) {
    // Ensure the submitted value matches the format produced by autocomplete.
    if (!preg_match('/^.+ \([0-9]+\)$/', $value_values)) {
      form_set_error(implode('][', $value_form['#parents']), t('The value must be a valid reference to an item of content.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function valueFormSubmit($value_values) {
    $this->value = $value_values;
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    $return = FALSE;

    if ($node = menu_get_object()) {
      $return = $node->nid === $this->getValueNid();
    }

    return $this->negate($return);
  }

  /**
   * Extract the nid from the set value.
   *
   * @return int|NULL
   *   The extracted NID or NULL if it cannot be extracted from the value.
   */
  protected function getValueNid() {
    preg_match('/^.+ \(([0-9]+)\)$/', $this->value, $matches);
    return isset($matches[1]) ? $matches[1] : NULL;
  }
}
