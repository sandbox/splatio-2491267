<?php

/**
 * @file
 * Contains BeanLogicPathCondition.
 */

/**
 * Class BeanLogicPathCondition.
 */
class BeanLogicPathCondition extends BeanLogicConditionPluginBase {

  /**
   * {@inheritdoc}
   */
  public function valueForm($form, &$form_state) {
    return array(
      '#type' => 'textfield',
      '#default_value' => $this->value,
      '#size' => 40,
      '#required' => TRUE,
    );
  }

  /**
   * {@inheritdoc}
   */
  public function valueFormSubmit($value_values) {
    $this->value = $value_values;
  }

  /**
   * {@inheritdoc}
   */
  public function execute() {
    return $this->negate(strpos(drupal_get_path_alias(), $this->value) !== FALSE);
  }
}
