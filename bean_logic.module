<?php

/**
 * @file
 * Hooks and functions for the Bean Logic module.
 */

/**
 * Implements hook_menu().
 */
function bean_logic_menu() {
  $items = array();

  $items['block/%bean_delta/placement'] = array(
    'title' => 'Placement',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('bean_logic_form', 1),
    'access callback' => 'bean_logic_placement_access',
    'access arguments' => array(1),
    'file' => 'bean_logic.admin.inc',
    'weight' => 0,
    'type' => MENU_LOCAL_TASK,
    'context' => MENU_CONTEXT_PAGE | MENU_CONTEXT_INLINE,
  );

  return $items;
}

/**
 * Access callback for campaign condition tab.
 */
function bean_logic_placement_access($bean) {
  $enabled_types = bean_logic_enabled_bean_types();
  return bean_access('update', $bean) && in_array($bean->type, $enabled_types);
}

/**
 * Implements hook_admin_paths().
 */
function bean_logic_admin_paths() {
  $paths = array(
    'block/*/placement' => TRUE,
    'block/*/placement/*' => TRUE,
  );

  return $paths;
}

/**
 * Implements hook_block_list_alter().
 */
/**
 * Implements hook_block_list_alter().
 */
function bean_logic_block_list_alter(&$blocks) {
  global $theme;

  foreach (bean_logic_enabled_bean_types() as $type) {
    $regions = bean_logic_get_regions($type);
    $beans = bean_load_multiple(FALSE, array(
      'type' => $type,
    ));
    foreach ($beans as $bean) {
      if (bean_logic_execute_bean_condition_set($bean->bid)) {
        $block = bean_logic_load_block_definition('bean', $bean->delta, $theme);
        $block->weight = 0;
        $block->region = isset($regions[$theme]) ? $regions[$theme] : BLOCK_REGION_NONE;
        $block->status = 1;

        $blocks[$block->bid] = $block;
      }
    }

  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function bean_logic_form_bean_admin_ui_type_form_alter(&$form, &$form_state) {
  $enabled_types = bean_logic_enabled_bean_types();
  $bean_type = $form['bean_type']['#value']->type;
  $form['bean_logic_enable'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow logic based placement'),
    '#default_value' => in_array($bean_type, $enabled_types),
  );

  // Region settings.
  $form['bean_logic_regions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default region'),
    '#collapsible' => FALSE,
    '#description' => t('Specify which region bean blocks should be placed in if not specifically placed.'),
    '#tree' => TRUE,
    '#states' => array(
      'invisible' => array(
        ':input[name="bean_logic_enable"]' => array('checked' => FALSE),
      ),
    ),
  );

  $regions = bean_logic_get_regions($bean_type);
  $theme_default = variable_get('theme_default', 'bartik');
  $admin_theme = variable_get('admin_theme');
  foreach (list_themes() as $key => $theme) {
    // Only display enabled themes.
    if ($theme->status) {
      // Use a meaningful title for the main site theme and administrative
      // theme.
      $theme_title = $theme->info['name'];
      if ($key == $theme_default) {
        $theme_title = t('!theme (default theme)', array('!theme' => $theme_title));
      }
      elseif ($admin_theme && $key == $admin_theme) {
        $theme_title = t('!theme (administration theme)', array('!theme' => $theme_title));
      }
      $form['bean_logic_regions'][$key] = array(
        '#type' => 'select',
        '#title' => $theme_title,
        '#default_value' => isset($regions[$key]) ? $regions[$key] : BLOCK_REGION_NONE,
        '#empty_value' => BLOCK_REGION_NONE,
        '#options' => system_region_list($key, REGIONS_VISIBLE),
        '#weight' => ($key == $theme_default ? 9 : 10),
      );
    }
  }

  $form['#submit'][] = 'bean_logic_bean_admin_ui_type_form_submit';
}

/**
 * Additional submit handler for bean type form.
 *
 * @param array $form
 *   The form being submitted.
 * @param array $form_state
 *   The state of the form being submitted.
 */
function bean_logic_bean_admin_ui_type_form_submit(array $form, array $form_state) {
  bean_logic_bean_type_save($form_state['values']['name'], (bool) $form_state['values']['bean_logic_enable']);
  bean_logic_bean_regions_save($form_state['values']['name'], $form_state['values']['bean_logic_regions']);
}

/**
 * Set the bean logic value for a bean type.
 *
 * @param string $type
 *   The bean type to set the enable value for.
 * @param bool $value
 *   TRUE if placement should be enabled, FALSE otherwise.
 */
function bean_logic_bean_type_save($type, $value) {
  $settings = variable_get('bean_logic_types', array());
  $settings[$type] = (bool) $value;
  variable_set('bean_logic_types', $settings);
}

/**
 * Get the bean types that have logic placement enabled.
 *
 * @return array
 *   An array of bean type machine names.
 */
function bean_logic_enabled_bean_types() {
  $settings = variable_get('bean_logic_types', array());
  $bean_types = bean_get_types();

  // Ensure only valid bean types are returned.
  $valid_settings = array_intersect_key($settings, $bean_types);

  // Only return types with a truthy value.
  $return = array();
  foreach ($valid_settings as $type => $value) {
    if ($value) {
      $return[] = $type;
    }
  }
  return $return;
}

/**
 * Save info on region placement for a bean type.
 *
 * @param string $type
 *   The bean type to save the region settings for.
 * @param array $regions
 *   An array of regions for each theme. The array should be keyed by theme name
 *   and the value should be a region in that theme.
 */
function bean_logic_bean_regions_save($type, array $regions) {
  variable_set('bean_logic_regions_' . $type, $regions);
}

/**
 * Get the region set for a specific bean type.
 *
 * @param string $type
 *   The bean type to get the regions for.
 *
 * @return array
 *   An array keyed by theme name with each value being the default region set
 *   for that theme.
 */
function bean_logic_get_regions($type) {
  return variable_get('bean_logic_regions_' . $type, array());
}

/**
 * Combine a condition set results by executing each condition.
 *
 * AND for each group and OR for each condition within the group.
 *
 * @param int $bean_id
 *   The bean ID to execute the condition set for.
 *
 * @return bool
 *   The result of combining the executed conditions.
 */
function bean_logic_execute_bean_condition_set($bean_id) {
  $condition_set = bean_logic_get_bean_condition_set($bean_id);
  // Loop through each group and then condition.
  foreach ($condition_set as $group) {
    // Default the result of combining the group condition to FALSE.
    $group_result = FALSE;
    foreach ($group as $condition) {
      // As soon as we find a condition in the group that returns TRUE, set the
      // group result and break - we don't need to check any other conditions
      // for this group.
      if ($condition->execute()) {
        $group_result = TRUE;
        break;
      }
    }

    // If any group result is FALSE return FALSE straight away as all groups
    // must be truthy.
    if (!$group_result) {
      return FALSE;
    }
  }

  // At this point, if none of the groups have been falsey, return TRUE.
  return TRUE;
}

/**
 * Get a bean condition set for a given bean.
 *
 * @param int $bean_id
 *   The ID of the bean to load the condition set for.
 *
 * @return array
 *   An array of groups, each being an array of condition objects.
 */
function bean_logic_get_bean_condition_set($bean_id) {
  $condition_set = array();
  $result = db_query('SELECT cid, gid FROM {bean_logic_conditions} WHERE bean_id = :bean_id ORDER BY cid', array(':bean_id' => $bean_id));
  foreach ($result as $record) {
    $condition_set[$record->gid][] = bean_logic_condition_plugin_load($record->cid);
  }
  return $condition_set;
}

/**
 * Load an instantiated plugin object representing a condition.
 *
 * @param int $cid
 *   The condition ID of the condition to load.
 *
 * @return BeanLogicConditionPluginBase|NULL
 *   The loaded condition object or NULL if it can not be loaded.
 */
function bean_logic_condition_plugin_load($cid) {
  // Fetch the condition being loaded.
  $result = db_query('SELECT cid, gid, bean_id, condition_plugin, value, negate FROM {bean_logic_conditions} WHERE cid = :cid', array(':cid' => $cid));
  $record = $result->fetchAssoc();
  if ($record) {
    return bean_logic_condition_plugin_create($record);
  }
  return NULL;
}

/**
 * Save the bean condition set into the database.
 *
 * @param array $condition_set
 *   The condition set being saved. The structure should be an array of groups
 *   each group being an array of condition objects.
 */
function bean_logic_save_bean_condition_set(array $condition_set) {
  foreach ($condition_set as $group_delta => $group) {
    foreach ($group as $condition) {
      $primary_keys = is_null($condition->cid) ? array() : 'cid';
      $condition->gid = $group_delta;
      $values = $condition->getValues();
      drupal_write_record('bean_logic_conditions', $values, $primary_keys);
    }
  }
}

/**
 * Create an instantiated plugin object representing a condition.
 *
 * @param array $record
 *   An array representing the data for the condition to load. Must contain the
 *   following keys:
 *   - cid: The condition ID of the condition, or NULL if the condition hasn't
 *       yet been committed to the database.
 *   - gid: The group ID this condition is within.
 *   - bean_id: The ID of the bean this condition is related to.
 *   - condition_plugin: The machine name of the condition plugin to use for
 *       this condition.
 *   - value: The value being checked.
 *   - negate: Whether to negate the condition.
 *
 * @return BeanLogicConditionPluginBase|NULL
 *   The loaded condition object or NULL if it can not be loaded.
 */
function bean_logic_condition_plugin_create(array $record) {
  // Load all plugin definitions.
  $definitions = bean_logic_condition_plugin_definitions();
  if (isset($definitions[$record['condition_plugin']])) {
    return new $definitions[$record['condition_plugin']]['class_name']($record['cid'], $record['gid'], $record['bean_id'], $record['condition_plugin'], $record['value'], $record['negate']);
  }
  return NULL;
}

/**
 * Get the defined condition plugin definitions.
 *
 * @return array
 *   The array of plugin definitions from enabled modules.
 *
 * @throws Exception
 *   When plugin doesn't extend BeanLogicConditionPluginBase.
 */
function bean_logic_condition_plugin_definitions() {
  $definitions = module_invoke_all('bean_logic_condition_plugin_info');
  foreach ($definitions as $machine_name => $definition) {
    if (!is_subclass_of($definition['class_name'], 'BeanLogicConditionPluginBase')) {
      throw new Exception(sprintf('The plugin class for the %s condition plugin does not exist or does not implement the required interface.', $machine_name));
    }
  }
  return $definitions;
}

/**
 * Get the human readable name for a given plugin.
 *
 * @param string $plugin_type
 *   The machine name of the plugin the get the label for.
 *
 * @return string
 *   The human readable name.
 */
function bean_logic_condition_plugin_definition_label($plugin_type) {
  $definitions = bean_logic_condition_plugin_definitions();
  if (isset($definitions[$plugin_type])) {
    return $definitions[$plugin_type]['label'];
  }
  return '';
}


function bean_logic_load_block_definition($module, $delta, $theme) {
  if (isset($delta)) {
    $block = db_query('SELECT * FROM {block} WHERE module = :module AND delta = :delta AND theme = :theme', array(
      ':module' => $module,
      ':delta' => $delta,
      ':theme' => $theme,
    ))->fetchObject();
  }

  // If the block does not exist in the database yet return a stub block
  // object.
  if (empty($block)) {
    $block = new stdClass();
    $block->module = $module;
    $block->delta = $delta;
    $block->theme = $theme;
  }

  return $block;
}
