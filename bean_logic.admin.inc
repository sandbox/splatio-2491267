<?php

/**
 * @file
 * Administration forms for bean logic.
 */

/**
 * Logic form callback to build a bean Logic form.
 *
 * @param array $form
 *   The base form array to build to form into.
 * @param array $form_state
 *   The current state of the form.
 * @param object $bean
 *   The related form to build the condition form for.
 *
 * @return array
 *   The build form, ready to render.
 */
function bean_logic_form(array $form, array &$form_state, $bean) {
  // Provide some help text to increase usability of the form.
  $form['help_text'] = array(
    '#markup' => '<p>' . t('Each group below can contain one or more conditions. Each group will be combined using AND to produce a true or false value based on the conditions.') . '</p>',
  );

  // We need to set #tree as we will need to know the hierarchy of the
  // the submitted values in form submit functions later.
  $form['#tree'] = TRUE;

  $form['#attributes'] = array('class' => 'bean-logic');

  // Set the bean BEAN_ID as a hidden value for retrieval in submit functions.
  $form['bean_id'] = array(
    '#type' => 'hidden',
    '#value' => $bean->bid,
  );

  // If we are loading this form for the first time, load the condition set into
  // form state.
  if (!isset($form_state['bean_logic_condition_set'])) {
    $form_state['bean_logic_condition_set'] = bean_logic_get_bean_condition_set($bean->bid);
  }

  // If the last group is populated, add an empty "new" group.
  $last_group = end($form_state['bean_logic_condition_set']);
  reset($form_state['bean_logic_condition_set']);
  if (!empty($last_group) || empty($form_state['bean_logic_condition_set'])) {
    $form_state['bean_logic_condition_set'][] = array();
  }

  // Loop through each group and create a fieldset for each.
  $group_iterator = 0;
  foreach ($form_state['bean_logic_condition_set'] as $group_delta => $group) {
    $group_form = array(
      '#type' => 'fieldset',
      '#title' => t('Any of'),
      '#description' => '<p>' . t('Each condition in this group will be combined using OR to produce a true or false value for the combination of conditions.') . '</p>',
    );

    // When looping through each condition, add to the group iterator.
    $group_iterator++;

    // For each condition, create a condition form group.
    foreach ($group as $condition_delta => $condition) {
      $condition_form = array(
        '#type' => 'container',
        '#attributes' => array(
          'class' => array(
            'container-inline',
            'bean-logic-container',
          ),
        ),
      );
      $condition_form['condition_type'] = array(
        '#markup' => bean_logic_condition_plugin_definition_label($condition->conditionPlugin) . ' ',
      );
      $condition_form['value'] = $condition->valueForm($form, $form_state);
      $condition_form['negate'] = array(
        '#type' => 'checkbox',
        '#title' => t('Negate'),
        '#default_value' => $condition->negate,
      );
      $condition_form['remove'] = array(
        '#type' => 'submit',
        '#value' => t('Remove'),
        '#submit' => array('bean_logic_form_condition_remove'),
        '#name' => 'remove_' . $group_delta . '_' . $condition_delta,
        '#limit_validation_errors' => array(),
      );

      // Add the condition form to this group fieldset.
      $group_form[$condition_delta] = $condition_form;
      if ($condition_delta != (count($group) - 1)) {
        $group_form[$condition_delta]['#suffix']  = '<div class="bean-logic-or">' . t('Or') . '</div>';
        $group_form['#attached']['css'][] = drupal_get_path('module', 'bean_logic') . '/bean-logic.css';
      }
    }

    // Add an additional "new" condition form for this group.
    $condition_form = array(
      '#type' => 'container',
      '#attributes' => array(
        'class' => array(
          'container-inline',
          'bean-logic-container',
          'bean-logic-container--add',
        ),
      ),
    );
    $condition_form['condition_type'] = array(
      '#type' => 'select',
      '#title' => t('New Condition'),
      '#options' => bean_logic_condition_options(),
      '#empty_option' => ' - ' . t('None') . ' - ',
      '#default_value' => '',
    );
    $condition_form['add'] = array(
      '#type' => 'submit',
      '#value' => t('Add'),
      '#submit' => array('bean_logic_form_rebuild'),
    );
    $group_form['new'] = $condition_form;

    // Add the group fieldset to the parent form.
    $form['conditions'][$group_delta] = $group_form;
    if ($group_iterator != count($form_state['bean_logic_condition_set'])) {
      $form['conditions'][$group_delta]['#suffix'] = '<div class="bean-logic-and">' . t('And') . '</div>';
      $form['#attached']['css'][] = drupal_get_path('module', 'bean_logic') . '/bean-logic.css';
    }
  }

  // Provide form actions.
  $form['save_end'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    '#weight' => 100,
  );

  return $form;
}

/**
 * Rebuild the store of conditions in form state based on the submitted values.
 *
 * Handler for the "Add" condition submit buttons.
 *
 * @param array $form
 *   The form structure being submitted.
 * @param array $form_state
 *   The form state of the form being submitted.
 * @param bool $ignore_new
 *   Whether to ignore any new conditions in submitted values.
 */
function bean_logic_form_rebuild(array &$form, array &$form_state, $ignore_new = FALSE) {
  // Get the bean BEAN_ID.
  $bean_id = $form_state['values']['bean_id'];

  // Loop through each group and condition.
  foreach ($form_state['values']['conditions'] as $group_delta => $group) {
    foreach ($group as $condition_delta => $condition) {
      // For any new items, instantiate a new condition and add it to the form
      // state.
      if ($condition_delta === 'new') {
        if ($ignore_new) {
          continue;
        }
        if (!empty($condition['condition_type'])) {
          $base_record = array(
            'cid' => NULL,
            'gid' => $group_delta,
            'bean_id' => $bean_id,
            'condition_plugin' => $condition['condition_type'],
            'value' => NULL,
            'negate' => 0,
          );
          $form_state['bean_logic_condition_set'][$group_delta][] = bean_logic_condition_plugin_create($base_record);
        }
      }
      else {
        // Set any updated values into the condition objects.
        $form_state['bean_logic_condition_set'][$group_delta][$condition_delta]->valueFormSubmit($condition['value']);
        $form_state['bean_logic_condition_set'][$group_delta][$condition_delta]->negate = $condition['negate'];
      }
    }
  }

  // Unset any input on the new condition elements to prevent the last submitted
  // value being set back as the default value.
  foreach ($form_state['input']['conditions'] as &$group) {
    $group['new']['condition_type'] = '';
  }

  // Ensure the form is rebuilt.
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit function for the condition "Remove" submit buttons.
 *
 * @param array $form
 *   The form being submitted.
 * @param array $form_state
 *   The form state of the submitted form.
 */
function bean_logic_form_condition_remove(array &$form, array &$form_state) {
  // Ensure the submit we expect has been triggered.
  $trigger = $form_state['triggering_element'];
  if (count($trigger['#parents']) == 4 && is_numeric($trigger['#parents'][1]) && is_numeric($trigger['#parents'][2])) {
    // Extract the group and condition deltas from the form parents.
    $group_delta = $trigger['#parents'][1];
    $condition_delta = $trigger['#parents'][2];

    // If the condition has a cid, and therefore has been committed to the
    // database then delete it.
    if ($cid = $form_state['bean_logic_condition_set'][$group_delta][$condition_delta]->cid) {
      db_delete('bean_logic_conditions')
        ->condition('cid', $cid)
        ->execute();
    }

    // Remove the condition from the form state so it doesn't appear on the
    // condition form.
    unset($form_state['bean_logic_condition_set'][$group_delta][$condition_delta]);
  }

  // If this group is now empty, clean it up.
  if (isset($group_delta) && empty($form_state['bean_logic_condition_set'][$group_delta])) {
    unset($form_state['bean_logic_condition_set'][$group_delta]);
  }

  // Set a message to confirm the removal and rebuild the form.
  drupal_set_message('Condition has been removed.');
  $form_state['rebuild'] = TRUE;
}

/**
 * Default validation handler for the condition form.
 *
 * @param array $form
 *   The form structure being submitted.
 * @param array $form_state
 *   The form state of the form being submitted.
 */
function bean_logic_form_validate(array $form, array &$form_state) {
  foreach ($form_state['bean_logic_condition_set'] as $group_delta => $group) {
    foreach ($group as $condition_delta => $condition) {
      $value_values = $form_state['values']['conditions'][$group_delta][$condition_delta]['value'];
      $value_form = $form['conditions'][$group_delta][$condition_delta]['value'];
      $condition->valueFormValidate($value_values, $value_form);
    }
  }
}

/**
 * Default submit handler for the condition form.
 *
 * @param array $form
 *   The form structure being submitted.
 * @param array $form_state
 *   The form state of the form being submitted.
 */
function bean_logic_form_submit(array &$form, array &$form_state) {
  // Rebuild the stored conditions.
  bean_logic_form_rebuild($form, $form_state, TRUE);

  // Override the rebuild set in bean_logic_form_rebuild() as we now
  // want to fully submit the form.
  $form_state['rebuild'] = FALSE;

  // Commit the condition set to the database.
  bean_logic_save_bean_condition_set($form_state['bean_logic_condition_set']);

  drupal_set_message(t('Bean placement has been updated.'));
}

/**
 * Get the select element options based on the available condition plugins.
 *
 * @return array
 *   An array of machine name labels, keyed by plugin machine name. Ready to be
 *   passed to #options.
 */
function bean_logic_condition_options() {
  $definitions = bean_logic_condition_plugin_definitions();
  $options = array();
  foreach ($definitions as $machine_name => $definition) {
    $options[$machine_name] = $definition['label'];
  }
  return $options;
}
