<?php

/**
 * @file
 * Contains BeanLogicConditionPluginBase.
 */

/**
 * Class BeanLogicConditionPluginBase.
 */
abstract class BeanLogicConditionPluginBase {

  /**
   * The condition ID.
   *
   * @var int
   */
  public $cid;

  /**
   * The condition group ID.
   *
   * @var int
   */
  public $gid;

  /**
   * The bean ID.
   *
   * @var int
   */
  public $bean_id;

  /**
   * The machine name of the condition plugin.
   *
   * @var string
   */
  public $conditionPlugin;

  /**
   * The value to check.
   *
   * @var mixed
   */
  public $value;

  /**
   * Whether to negate the condition.
   *
   * @var int
   */
  public $negate;

  /**
   * Constructs a new condition plugin.
   *
   * @param int $cid
   *   The condition ID.
   * @param int $gid
   *   The group ID.
   * @param int $bean_id
   *   The bean ID.
   * @param string $condition_plugin
   *   The machine name of the condition plugin.
   * @param mixed $value
   *   The value to check.
   * @param int $negate
   *   Whether to negate the condition.
   */
  public function __construct($cid, $gid, $bean_id, $condition_plugin, $value, $negate) {
    $this->cid = $cid;
    $this->gid = $gid;
    $this->bean_id = $bean_id;
    $this->conditionPlugin = $condition_plugin;
    $this->value = $value;
    $this->negate = $negate;
  }

  /**
   * Get the form to record the value for this condition.
   *
   * @param array $form
   *   The form being built.
   * @param array $form_state
   *   The form state of the form being built.
   *
   * @return array
   *   The FAPI form elements to capture the value.
   */
  abstract public function valueForm($form, &$form_state);

  /**
   * Take the submitted values and save them within the condition.
   *
   * @param array|string $value_values
   *   The form values returned by the submission of the value form.
   */
  abstract public function valueFormSubmit($value_values);

  /**
   * Validate the submitted values.
   *
   * @param array|string  $value_values
   *   The form values returned by the submission of the value form.
   * @param array $value_form
   *   The form structure being validated.
   */
  public function valueFormValidate($value_values, $value_form) {}

  /**
   * Run the condition with the set value to return a truthy value.
   *
   * @return bool
   *   TRUE or FALSE depending on the condition value being checked.
   */
  abstract public function execute();

  /**
   * Get the values of the condition being represented.
   *
   * @return array
   *   An array of the data stored within this condition object:
   *   - cid: The condition ID of the condition, or NULL if the condition hasn't
   *       yet been committed to the database.
   *   - gid: The group ID this condition is within.
   *   - bean_id: The ID of the bean this condition is related to.
   *   - condition_plugin: The machine name of the condition plugin to use for
   *       this condition.
   *   - value: The value being checked.
   *   - negate: Whether to negate the condition.
   */
  public function getValues() {
    return array(
      'cid' => $this->cid,
      'gid' => $this->gid,
      'bean_id' => $this->bean_id,
      'condition_plugin' => $this->conditionPlugin,
      'value' => $this->value,
      'negate' => $this->negate,
    );
  }

  /**
   * Negate the passed value if the negate property is truthy.
   *
   * @param bool $value
   *   The value to negate.
   *
   * @return bool
   *   The value or negated value.
   */
  protected function negate($value) {
    return $this->negate ? !$value : $value;
  }
}
